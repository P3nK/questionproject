﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Mvc;
using QuestionTest.Models;

namespace QuestionTest.Controllers
{
    public class HomeController:Controller
    {
        #region View

        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            using(var _context = new QuestionDBEntities())
            {
                var result = _context.usp_Answer_Select(1).ToList();
                ViewBag.answer = result;
            }

            return View();
        }

        #endregion View
    }
}